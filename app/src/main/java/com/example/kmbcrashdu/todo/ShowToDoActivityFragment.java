package com.example.kmbcrashdu.todo;

import android.accounts.Account;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.example.kmbcrashdu.todo.data.ToDoContract;
import com.example.kmbcrashdu.todo.sync.ToDoSyncAdapter;

/**
 * A placeholder fragment containing a simple view.
 */
public class ShowToDoActivityFragment extends Fragment {

    public ShowToDoActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_show_to_do, container, false);
        Intent callingIntent = getActivity().getIntent();
        final String _id = callingIntent.getStringExtra("_id");

        final Cursor cursor = getContext().getContentResolver().query(ToDoContract.ToDoEntries.CONTENT_URI, null, ToDoContract.ToDoEntries._ID + "=?", new String[]{_id}, null);
        try {
            cursor.moveToFirst();
        }
        catch (NullPointerException e)
        {
            Log.e("Error", e.getMessage());
        }
        final String id = Integer.toString(cursor.getInt(4));

        final CheckBox checkBox = (CheckBox)view.findViewById(R.id.show_to_do_done_checkbox);
        checkBox.setChecked(cursor.getInt(MainActivityFragment.COL_TODO_DONE) > 0);

        final EditText title = (EditText) view.findViewById(R.id.show_to_do_title_edittext);
        title.setText(cursor.getString(MainActivityFragment.COL_TODO_TITLE));

        final EditText description = (EditText) view.findViewById(R.id.show_to_do_description_edittext);
        description.setText(cursor.getString(MainActivityFragment.COL_TODO_DESC));


        //PATCH/PUT
        Button save = (Button)view.findViewById(R.id.show_to_do_save_button);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContentValues todoVals = new ContentValues();
                todoVals.put(ToDoContract.ToDoEntries.TODO_TITLE, title.getText().toString());
                todoVals.put(ToDoContract.ToDoEntries.TODO_DESC, description.getText().toString());
                todoVals.put(ToDoContract.ToDoEntries.TODO_DONE, checkBox.isChecked()? 1 : 0);
                getContext().getContentResolver().update(ToDoContract.ToDoEntries.CONTENT_URI, todoVals, ToDoContract.ToDoEntries._ID + "=?", new String[]{_id});
                updateSync(id, title.getText().toString(), description.getText().toString(), checkBox.isChecked()? 1 : 0);
                Toast.makeText(getContext(),"ToDo updated", Toast.LENGTH_LONG).show();
            }
        });

        //Call delete on todos with specified id
        Button delete = (Button)view.findViewById(R.id.show_to_do_delete_button);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.v("Server id", id);
                getContext().getContentResolver().delete(ToDoContract.ToDoEntries.CONTENT_URI, ToDoContract.ToDoEntries._ID + "=?", new String[]{_id});
                deleteSync(id);
                getActivity().finish();
            }
        });
        cursor.close();
        return view;
    }

    public void updateSync(String id, String title, String description, int done)
    {
        Bundle bundle = new Bundle();
        bundle.putString("server_id", id);
        bundle.putString("httpverb", "PATCH");
        bundle.putString("title", title);
        bundle.putString("description", description);
        bundle.putInt("done", done);
        ToDoSyncAdapter.syncImmediately((Account)MainActivity.universalBundle.getParcelable("account"), ToDoContract.CONTENT_AUTHORITY, bundle);

    }

    public void deleteSync(String id)
    {
        Bundle bundle = new Bundle();
        bundle.putString("server_id", id);
        bundle.putString("httpverb", "DELETE");
        ToDoSyncAdapter.syncImmediately((Account)MainActivity.universalBundle.getParcelable("account"), ToDoContract.CONTENT_AUTHORITY, bundle);
    }
}
