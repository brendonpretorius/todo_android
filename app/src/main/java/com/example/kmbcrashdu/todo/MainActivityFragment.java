package com.example.kmbcrashdu.todo;

import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.kmbcrashdu.todo.data.ToDoContract;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>{

    public static final int COL_TODO_ID = 0;
    public static final int COL_TODO_TITLE = 1;
    public static final int COL_TODO_DESC = 2;
    public static final int COL_TODO_DONE = 3;
    public static final int COL_TODO_SERVER_ID = 4;

    private static final int LOADER = 1;

    private ToDoCursorAdapter todoCursorAdapter;
    public MainActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        todoCursorAdapter = new ToDoCursorAdapter(getContext(),null, 0);

        View view = inflater.inflate(R.layout.fragment_main, container, false);

        ListView lv = (ListView)view.findViewById(R.id.fragment_main_ListView);
        lv.setAdapter(todoCursorAdapter);


        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        getLoaderManager().initLoader(LOADER, null, this);

        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Uri uri = ToDoContract.ToDoEntries.CONTENT_URI;
        return new CursorLoader(getContext(),uri, null, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        todoCursorAdapter.changeCursor(data);

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        todoCursorAdapter.swapCursor(null);
    }


}
