package com.example.kmbcrashdu.todo.data;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;


public class ToDoContract {

    public static final String CONTENT_AUTHORITY = "com.example.kmbcrashdu.todo";
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" +CONTENT_AUTHORITY);
    public static final String PATH = "todo";

    public static final class ToDoEntries implements BaseColumns
    {
        public static final String TABLE_NAME = "todo";
        public static final String TODO_TITLE = "todo_title";
        public static final String TODO_DESC = "todo_description";
        public static final String TODO_DONE = "todo_done";
        public static final String TODO_SERVER_ID = "todo_server_id";

        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH).build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH;



        public static Uri buildIDUri(long id)
        {
            return ContentUris.withAppendedId(CONTENT_URI,id);
        }
    }

}
