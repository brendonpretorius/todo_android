package com.example.kmbcrashdu.todo.sync;

import android.accounts.Account;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.SyncResult;
import android.os.Bundle;
import android.util.Log;

import com.example.kmbcrashdu.todo.data.ToDoContract;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Vector;

public class ToDoSyncAdapter extends AbstractThreadedSyncAdapter{

    ContentResolver mContentResolver;
    /**
     * Set up the sync adapter
     */
    public ToDoSyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        /*
         * If your app uses a content resolver, get an instance of it
         * from the incoming Context
         */
        mContentResolver = context.getContentResolver();
    }

    /**
     * Set up the sync adapter. This form of the
     * constructor maintains compatibility with Android 3.0
     * and later platform versions
     */
    public ToDoSyncAdapter(
            Context context,
            boolean autoInitialize,
            boolean allowParallelSyncs) {
        super(context, autoInitialize, allowParallelSyncs);
        /*
         * If your app uses a content resolver, get an instance of it
         * from the incoming Context
         */
        mContentResolver = context.getContentResolver();
    }

    /*
    * Specify the code you want to run in the sync adapter. The entire
    * sync adapter runs in a background thread, so you don't have to set
    * up your own background processing.
    */
    @Override
    public void onPerformSync(
            Account account,
            Bundle extras,
            String authority,
            ContentProviderClient provider,
            SyncResult syncResult) {





        //String action = extras.getString("action");
        String action = extras.getString("httpverb");
        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;

        String ToDoJSONString;
        String format = "json";
        final String BASE_URL = "http://glacial-falls-31685.herokuapp.com/api/todos";
        try {
            if(action.equalsIgnoreCase("GET")){
                try {
                    URL url = new URL(BASE_URL + "." + format);

                    urlConnection = (HttpURLConnection) url.openConnection();
                    urlConnection.setRequestMethod("GET");
                    urlConnection.connect();

                    InputStream inputStream = urlConnection.getInputStream();
                    StringBuilder buffer = new StringBuilder();
                    if (inputStream == null) {
                        // Nothing to do.
                        return;
                    }
                    reader = new BufferedReader(new InputStreamReader(inputStream));

                    String line;
                    while ((line = reader.readLine()) != null) {
                        // Since it's JSON, adding a newline isn't necessary (it won't affect parsing)
                        // But it does make debugging a *lot* easier if you print out the completed
                        // buffer for debugging.
                        buffer.append(line);
                    }

                    if (buffer.length() == 0) {
                        // Stream was empty.  No point in parsing.
                        return ;
                    }
                    ToDoJSONString = buffer.toString();
                    getTodosFromJSON(ToDoJSONString);
                }
                catch (IOException e) {
                    Log.e("TODO", "Error in GET", e);
                    // If the code didn't successfully get the weather data, there's no point in attempting
                    // to parse it.

                }
                catch (JSONException e) {
                    e.printStackTrace();
                }
                finally {
                    if (urlConnection != null) {
                        urlConnection.disconnect();
                    }
                    if (reader != null) {
                        try {
                            reader.close();
                        } catch (final IOException e) {
                            Log.e("TODO", "Error closing stream", e);
                        }
                    }
                }
            }
            else if(action.equalsIgnoreCase("POST")) {
                try {

                    URL url = new URL(BASE_URL + "." + format);

                    urlConnection = (HttpURLConnection) url.openConnection();
                    urlConnection.setRequestMethod("POST");
                    urlConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                    urlConnection.setDoOutput(true);
                    //urlConnection.connect();

                    JSONObject todo = new JSONObject();
                    try {
                        todo.put("description", extras.getString("desc"));
                        todo.put("title", extras.getString("title"));
                        todo.put("done", 0);

                    } catch (JSONException e) {
                        // Auto-generated catch block
                        e.printStackTrace();
                    }




                    JSONObject todoObj = new JSONObject();
                    todoObj.put("todo", todo);



                    String jsonStr = todoObj.toString();
                    Log.d("JSON OBJECT:", jsonStr);





                    BufferedWriter out =
                            new BufferedWriter(new OutputStreamWriter(urlConnection.getOutputStream()));
                    out.write(jsonStr);
                    out.close();

                    urlConnection.connect();
                    int HttpResult = urlConnection.getResponseCode();
                    Log.d("RESPONSE: ", Integer.toString(HttpResult));

                    BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    StringBuilder response = new StringBuilder();
                    String line;
                    while((line = br.readLine()) != null)
                    {
                        response.append(line);
                    }



                    JSONObject responseJSON = new JSONObject(response.toString());
                    int server_id = responseJSON.getInt("id");
                    ContentValues updateVals = new ContentValues();
                    updateVals.put(ToDoContract.ToDoEntries.TODO_SERVER_ID, server_id);
                    getContext().getContentResolver().update(ToDoContract.ToDoEntries.CONTENT_URI, updateVals, ToDoContract.ToDoEntries.TODO_SERVER_ID + "=?", new String[]{"0"});


                } catch (Exception e) {
                    Log.e("TODO", "Error in POST", e);
                }
            }

            else if(action.equalsIgnoreCase("PATCH"))
            {

                try{
                    URL url = new URL(BASE_URL + "/" + extras.getString("server_id") + "." + format);
                    urlConnection = (HttpURLConnection) url.openConnection();
                    urlConnection.setRequestMethod("PUT");
                    urlConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                    urlConnection.setDoOutput(true);


                    JSONObject todo = new JSONObject();
                    JSONObject todoObject = new JSONObject();
                    try {
                        todo.put("description", extras.getString("description"));
                        todo.put("title", extras.getString("title"));
                        todo.put("done", extras.getInt("done"));

                        todoObject.put("todo", todo);

                        Log.d("Todo:", todoObject.toString());

                    } catch (JSONException e) {
                        // Auto-generated catch block
                        e.printStackTrace();
                    }



                    BufferedWriter out =
                            new BufferedWriter(new OutputStreamWriter(urlConnection.getOutputStream()));
                    out.write(todoObject.toString());
                    out.close();

                    urlConnection.connect();
                    int HttpResult = urlConnection.getResponseCode();


                }
                catch (Exception e)
                {
                    Log.e("TODO", "Error in PATCH", e);
                }
            }

            else if (action.equalsIgnoreCase("DELETE"))
            {
                try{
                    URL url = new URL(BASE_URL + "/" + extras.getString("server_id") + "." + format);

                    urlConnection = (HttpURLConnection) url.openConnection();
                    urlConnection.setRequestMethod("DELETE");
                    urlConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");

                    urlConnection.connect();

                    int HttpResult = urlConnection.getResponseCode();

                }
                catch (Exception e)
                {
                    Log.e("TODO", "Error in DELETE", e);
                }

            }

            else if (action.equalsIgnoreCase("destroy_all_done"))
            {
                try{
                    URL url = new URL(BASE_URL + "_done" + "." + format);

                    urlConnection = (HttpURLConnection) url.openConnection();
                    urlConnection.setRequestMethod("DELETE");
                    urlConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");

                    urlConnection.connect();

                    int HttpResult = urlConnection.getResponseCode();


                }
                catch (Exception e)
                {
                    Log.e("TODO", "Error in destroy_all_done", e);
                }
            }

        }
        catch (NullPointerException e)
        {
            Log.e("Error", e.getMessage());
        }


    }

    private void getTodosFromJSON(String jsonString) throws JSONException
    {

        try
        {
            JSONArray jsonArray = new JSONArray(jsonString);


            Vector<ContentValues> contentValuesVector = new Vector<>(jsonArray.length());

            for (int i = 0; i < jsonArray.length(); i++)
            {
                int done;
                String title;
                String description;
                int server_id;

                JSONObject todo = jsonArray.getJSONObject(i);

                done  = todo.getBoolean("done")? 1 : 0;
                title = todo.getString("title");
                description = todo.getString("description");
                server_id = todo.getInt("id");


                ContentValues cv = new ContentValues();
                cv.put(ToDoContract.ToDoEntries.TODO_DONE, done);
                cv.put(ToDoContract.ToDoEntries.TODO_DESC, description);
                cv.put(ToDoContract.ToDoEntries.TODO_TITLE, title);
                cv.put(ToDoContract.ToDoEntries.TODO_SERVER_ID, server_id);
                contentValuesVector.add(cv);
            }


            if(contentValuesVector.size() > 0)
            {
                ContentValues[] cvArray = new ContentValues[contentValuesVector.size()];
                contentValuesVector.toArray(cvArray);

                mContentResolver.bulkInsert(ToDoContract.ToDoEntries.CONTENT_URI, cvArray);

            }

        }
        catch (Exception e)
        {
            Log.e("ERROR", e.getMessage());
        }


    }

    public static void syncImmediately(Account account, String content_authority, Bundle bundle) {
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
        ContentResolver.requestSync(account, content_authority, bundle);


    }
}
