package com.example.kmbcrashdu.todo.data;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;


public class ToDoProvider extends ContentProvider {

    private static final UriMatcher uriMatcher = buildUriMatcher();
    private static final SQLiteQueryBuilder sqlQueryBuilder;
    private ToDoDBHelper toDoDBHelper;
    private static final int TODO_ENTRIES = 100;

    static {
        sqlQueryBuilder = new SQLiteQueryBuilder();
        sqlQueryBuilder.setTables(ToDoContract.ToDoEntries.TABLE_NAME);
    }

    public static UriMatcher buildUriMatcher()
    {
        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        matcher.addURI(ToDoContract.CONTENT_AUTHORITY, ToDoContract.PATH, TODO_ENTRIES);
        return matcher;
    }



    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        final int toMatch = uriMatcher.match(uri);

        switch (toMatch)
        {
            case TODO_ENTRIES:
                return ToDoContract.ToDoEntries.CONTENT_TYPE;
            default:
                throw new UnsupportedOperationException("Unknown Uri: " + uri);
        }

    }

    @Override
    public int update(@NonNull Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        final SQLiteDatabase db = toDoDBHelper.getWritableDatabase();
        final int match = uriMatcher.match(uri);
        int rowsUpdated;

        switch (match)
        {
            case TODO_ENTRIES:
                rowsUpdated = db.update(ToDoContract.ToDoEntries.TABLE_NAME, values, selection, selectionArgs);
                break;
            default:
                throw new UnsupportedOperationException("Unknown Uri: " + uri);
        }
        if(rowsUpdated!=0)
            try
            {
                getContext().getContentResolver().notifyChange(uri, null);
            }
            catch (NullPointerException e)
            {
                Log.e("Error", e.getMessage());
            }

        return rowsUpdated;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, ContentValues values) {
        SQLiteDatabase db = toDoDBHelper.getWritableDatabase();
        int match = uriMatcher.match(uri);
        Uri returnUri;

        switch (match)
        {
            case TODO_ENTRIES:
                long _id = db.insert(ToDoContract.ToDoEntries.TABLE_NAME, null, values);
                if(_id > 0)
                    returnUri = ToDoContract.ToDoEntries.buildIDUri(_id);
                else
                    throw new SQLException("Failed to insert row " + uri);
                break;

            default:
                throw new UnsupportedOperationException("Unknown Uri: " + uri + "expected URI: " + ToDoContract.CONTENT_AUTHORITY + ToDoContract.PATH);

        }
        try
        {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        catch (NullPointerException e)
        {
            Log.e("Error", e.getMessage());
        }

        return returnUri;
    }

    @Override
    public int bulkInsert(@NonNull Uri uri, @NonNull ContentValues[] values) {
        SQLiteDatabase db = toDoDBHelper.getWritableDatabase();
        int match = uriMatcher.match(uri);
        switch (match)
        {
            case TODO_ENTRIES:
                db.beginTransaction();
                int returnCount = 0;
                try {
                    for(ContentValues value : values)
                    {
                        long _id = db.insert(ToDoContract.ToDoEntries.TABLE_NAME, null, value);
                        if(_id != 1)
                            returnCount++;
                    }
                    db.setTransactionSuccessful();
                }
                finally {
                    db.endTransaction();
                }
                try
                {
                    getContext().getContentResolver().notifyChange(uri, null);
                }
                catch (NullPointerException e)
                {
                    Log.e("Error", e.getMessage());
                }


                return returnCount;
            default:
                return super.bulkInsert(uri, values);
        }

    }

    @Override
    public boolean onCreate() {
        toDoDBHelper = new ToDoDBHelper(getContext());
        return true;
    }

    @Override
    public int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {
        SQLiteDatabase db = toDoDBHelper.getWritableDatabase();
        final int match = uriMatcher.match(uri);
        int rowsDeleted;
        switch (match)
        {
            case TODO_ENTRIES:
                rowsDeleted = db.delete(ToDoContract.ToDoEntries.TABLE_NAME, selection, selectionArgs);
                break;
            default:
                throw new UnsupportedOperationException("Unknown Uri: " + uri);
        }
        if(rowsDeleted != 0)
            try
            {
                getContext().getContentResolver().notifyChange(uri, null);
            }
            catch (NullPointerException e)
            {
                Log.e("Error", e.getMessage());
            }

        return rowsDeleted;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Cursor returnCursor = sqlQueryBuilder.query(toDoDBHelper.getReadableDatabase(), projection, selection, selectionArgs,null, null, sortOrder);
        try {
            returnCursor.setNotificationUri(getContext().getContentResolver(), uri);
        }
        catch (NullPointerException e)
        {
            Log.e("Error", e.getMessage());
        }
        return returnCursor;
    }


}
