package com.example.kmbcrashdu.todo;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.kmbcrashdu.todo.data.ToDoContract;
import com.example.kmbcrashdu.todo.sync.ToDoSyncAdapter;

public class MainActivity extends AppCompatActivity {

    // Constants
    // The authority for the sync adapter's content provider
    public static final String AUTHORITY = ToDoContract.CONTENT_AUTHORITY;
    // An account type, in the form of a domain name
    public static final String ACCOUNT_TYPE = "example.com";
    // The account name
    public static final String ACCOUNT = "dummyaccount";

    // Instance fields
    Account mAccount;
    // Global variables
    // A content URI for the content provider's data table

    ContentResolver mResolver;

    public static Bundle universalBundle = new Bundle();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mAccount = CreateSyncAccount(this);

        mResolver = getContentResolver();
        universalBundle.putParcelable("account", mAccount);
        sync();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if(id == R.id.action_delete_todos)
        {
            removeSelected();
            return true;
        }

        if(id == R.id.action_new_todo)
        {
            createNew();
            return true;
        }

        if(id == R.id.action_sync)
        {

            sync();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void sync()
    {
        Bundle bundle = new Bundle();
        mResolver.delete(ToDoContract.ToDoEntries.CONTENT_URI, null, null);
        bundle.putString("httpverb", "GET");
        ToDoSyncAdapter.syncImmediately(mAccount, AUTHORITY, bundle);
        Log.d("MainActivity", "Call sync");
    }


    public void removeSelected()
    {
        //call delete_all_done on server api
        getContentResolver().delete(ToDoContract.ToDoEntries.CONTENT_URI, ToDoContract.ToDoEntries.TODO_DONE + "=?", new String[]{"1"});
        Bundle bundle = new Bundle();
        bundle.putString("httpverb", "destroy_all_done");
        ToDoSyncAdapter.syncImmediately(mAccount, AUTHORITY, bundle);
        Toast.makeText(this,"Deleted Selected ones", Toast.LENGTH_LONG).show();
        Log.d("Main", "delete all done");
    }

    public void createNew()
    {
        Intent showNew = new Intent(this, CreateNewTaskActivity.class);
        showNew.putExtra("account", mAccount);
        startActivity(showNew);
    }

    /**
     * Create a new dummy account for the sync adapter
     *
     * @param context The application context
     */
    public static Account CreateSyncAccount(Context context) {
        // Create the account type and default account
        Account newAccount = new Account(
                ACCOUNT, ACCOUNT_TYPE);
        // Get an instance of the Android account manager
        AccountManager accountManager =
                (AccountManager) context.getSystemService(
                        ACCOUNT_SERVICE);
        /*
         * Add the account and account type, no password or user data
         * If successful, return the Account object, otherwise report an error.
         */
        if ( null == accountManager.getPassword(newAccount) ) {

        /*
         * Add the account and account type, no password or user data
         * If successful, return the Account object, otherwise report an error.
         */
            if (!accountManager.addAccountExplicitly(newAccount, "", null)) {
                return null;
            }
            /*
             * If you don't set android:syncable="true" in
             * in your <provider> element in the manifest,
             * then call ContentResolver.setIsSyncable(account, AUTHORITY, 1)
             * here.
             */
            ContentResolver.setIsSyncable(newAccount, AUTHORITY, 1);

        }

        return newAccount;
    }



}
