package com.example.kmbcrashdu.todo;

import android.accounts.Account;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.example.kmbcrashdu.todo.data.ToDoContract;
import com.example.kmbcrashdu.todo.sync.ToDoSyncAdapter;


public class ToDoCursorAdapter extends CursorAdapter{

    public ToDoCursorAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_item, parent, false);
        CheckBox checkBox = (CheckBox)view.findViewById(R.id.row_item_done);
        checkBox.setTag(cursor.getInt(MainActivityFragment.COL_TODO_ID));
        return view;
    }

    @Override
    public void bindView(View view, final Context context, final Cursor cursor) {
        final CheckBox checkBox = (CheckBox)view.findViewById(R.id.row_item_done);
        checkBox.setTag(cursor.getInt(MainActivityFragment.COL_TODO_ID));
        checkBox.setChecked(cursor.getInt(MainActivityFragment.COL_TODO_DONE) > 0);

        checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int checkedInt = 0;
                ContentValues todoVals = new ContentValues();
                if(checkBox.isChecked())
                    checkedInt = 1;
                todoVals.put(ToDoContract.ToDoEntries.TODO_DONE, checkedInt);
                Log.v("TODO", todoVals.getAsInteger(ToDoContract.ToDoEntries.TODO_DONE).toString());
                Log.v("CursorPos", checkBox.getTag().toString());
                context.getContentResolver().update(ToDoContract.ToDoEntries.CONTENT_URI, todoVals, ToDoContract.ToDoEntries._ID + "=?", new String[]{checkBox.getTag().toString() });
                updateSync(checkBox.getTag().toString(), context);
            }
        });

        TextView textView = (TextView)view.findViewById(R.id.row_item_todo_title);
        textView.setText(cursor.getString(MainActivityFragment.COL_TODO_TITLE));
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent showToDo = new Intent(context, ShowToDoActivity.class);
                showToDo.putExtra("_id", checkBox.getTag().toString());
                context.startActivity(showToDo);
            }
        });
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return super.getView(position, convertView, parent);
    }

    public void updateSync(String id, Context context)
    {

        Cursor cursor = context.getContentResolver().query(ToDoContract.ToDoEntries.CONTENT_URI, null, ToDoContract.ToDoEntries._ID + "=?", new String[]{id}, null);
        try {
            cursor.moveToFirst();
        }
        catch (NullPointerException e)
        {
            Log.e("Error", e.getMessage());
        }

        Bundle bundle = new Bundle();
        bundle.putString("server_id", cursor.getString(MainActivityFragment.COL_TODO_SERVER_ID));
        bundle.putString("httpverb", "PATCH");
        bundle.putString("title", cursor.getString(MainActivityFragment.COL_TODO_TITLE));
        bundle.putString("description", cursor.getString(MainActivityFragment.COL_TODO_DESC));
        bundle.putInt("done", cursor.getInt(MainActivityFragment.COL_TODO_DONE));
        cursor.close();
        ToDoSyncAdapter.syncImmediately((Account)MainActivity.universalBundle.getParcelable("account"), ToDoContract.CONTENT_AUTHORITY, bundle);
    }
}
