package com.example.kmbcrashdu.todo.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class ToDoDBHelper extends SQLiteOpenHelper{
    public static final int DBVERSION = 1;
    public static final String DBNAME = "todo.db";

    public ToDoDBHelper(Context context) {
        super(context, DBNAME, null, DBVERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        final String CREATE_TABLE = "CREATE TABLE " + ToDoContract.ToDoEntries.TABLE_NAME + " (" +
                ToDoContract.ToDoEntries._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                ToDoContract.ToDoEntries.TODO_TITLE + " TEXT NOT NULL, " +
                ToDoContract.ToDoEntries.TODO_DESC + " TEXT NOT NULL, " +
                ToDoContract.ToDoEntries.TODO_DONE + " INTEGER NOT NULL, " +
                ToDoContract.ToDoEntries.TODO_SERVER_ID + " INTEGER NOT NULL);";

        db.execSQL(CREATE_TABLE);
    }



    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + ToDoContract.ToDoEntries.TABLE_NAME);
        onCreate(db);
    }
}
