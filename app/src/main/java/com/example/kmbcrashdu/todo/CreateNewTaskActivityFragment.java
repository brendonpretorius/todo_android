package com.example.kmbcrashdu.todo;

import android.accounts.Account;
import android.content.ContentValues;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kmbcrashdu.todo.data.ToDoContract;
import com.example.kmbcrashdu.todo.sync.ToDoSyncAdapter;

/**
 * A placeholder fragment containing a simple view.
 */
public class CreateNewTaskActivityFragment extends Fragment {

    public CreateNewTaskActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_create_new_task, container, false);

        final TextView title = (TextView)view.findViewById(R.id.create_new_task_title_text_edit);
        final TextView desc = (TextView)view.findViewById(R.id.create_new_task_desc_text_edit);

        Button saveButton = (Button)view.findViewById(R.id.create_new_task_save_button);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContentValues todoVals = new ContentValues();
                todoVals.put(ToDoContract.ToDoEntries.TODO_TITLE, title.getText().toString());
                todoVals.put(ToDoContract.ToDoEntries.TODO_DESC, desc.getText().toString());
                todoVals.put(ToDoContract.ToDoEntries.TODO_DONE, 0);
                todoVals.put(ToDoContract.ToDoEntries.TODO_SERVER_ID, 0);
                getContext().getContentResolver().insert(ToDoContract.ToDoEntries.CONTENT_URI, todoVals);
                Toast.makeText(getContext(),"ToDo created", Toast.LENGTH_LONG).show();
                sync(title.getText().toString(), desc.getText().toString());
                getActivity().finish();
            }
        });
        return view;
    }

    public void sync(String title, String desc)
    {
        Intent callingIntent = getActivity().getIntent();
        Bundle bundle = new Bundle();
        bundle.putString("httpverb", "POST");
        bundle.putString("title", title);
        bundle.putString("desc", desc);
        Account account = callingIntent.getParcelableExtra("account");

        ToDoSyncAdapter.syncImmediately(account, ToDoContract.CONTENT_AUTHORITY, bundle);
        Log.d("MainActivity", "Call sync");
    }
}
